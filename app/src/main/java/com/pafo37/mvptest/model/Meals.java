package com.pafo37.mvptest.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Meals {
    private List<Meal> meals = null;
    private Map<String, Object> additionalProperties = new HashMap<>();

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
