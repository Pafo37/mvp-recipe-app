package com.pafo37.mvptest.model;

public class Meal {
    private String idMeal;
    private String strMeal;
    private String strArea;
    private String strInstructions;

    public Meal(String idMeal, String strMeal, String strArea, String strInstructions) {
        this.idMeal = idMeal;
        this.strMeal = strMeal;
        this.strArea = strArea;
        this.strInstructions = strInstructions;
    }

    public String getIdMeal() {
        return idMeal;
    }

    public void setIdMeal(String idMeal) {
        this.idMeal = idMeal;
    }

    public String getStrMeal() {
        return strMeal;
    }

    public void setStrMeal(String strMeal) {
        this.strMeal = strMeal;
    }

    public String getStrArea() {
        return strArea;
    }

    public void setStrArea(String strArea) {
        this.strArea = strArea;
    }

    public String getStrInstructions() {
        return strInstructions;
    }

    public void setStrInstructions(String strInstructions) {
        this.strInstructions = strInstructions;
    }
}
