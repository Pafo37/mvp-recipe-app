package com.pafo37.mvptest.api;

import android.os.AsyncTask;

import com.pafo37.mvptest.model.Meal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class FetchMealTask extends AsyncTask<String, Integer, Meal> {
    private final IFetchMeal iFetchMeal;

    public FetchMealTask(IFetchMeal iFetchMeal) {
        this.iFetchMeal = iFetchMeal;
    }

    @Override
    protected Meal doInBackground(String... params) {
        String my_url = "https://www.themealdb.com/api/json/v1/1/search.php?s=" + params[0];
        BufferedReader reader = null;
        try {
            URL url = new URL(my_url);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();
            if (connection.getResponseCode() != 200) {
                return null;
            }
            InputStream inputStream = connection.getInputStream();
            StringBuffer sb = new StringBuffer();
            if (inputStream == null) {
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");

            }
            if (sb.length() == 0) {
                return null;
            }
            String response = sb.toString();
            return deserializeResponse(response);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onPostExecute(Meal meal) {
        super.onPostExecute(meal);
        if (meal != null) {
            iFetchMeal.onLoadComplete(meal);
        }
    }

    private Meal deserializeResponse(String response) {
        Meal meal = null;
        try {
            JSONObject responseJson = new JSONObject(response);
            JSONArray jsonArray = responseJson.getJSONArray("meals");

            JSONObject mealJson = jsonArray.getJSONObject(0);
            String idMeal = (String) mealJson.get("idMeal");
            String strMeal = (String) mealJson.get("strMeal");
            String strArea = (String) mealJson.get("strArea");
            String strInstructions = (String) mealJson.get("strInstructions");
            meal = new Meal(idMeal, strMeal, strArea, strInstructions);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return meal;
    }
}
