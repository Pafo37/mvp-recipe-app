package com.pafo37.mvptest.api;

import com.pafo37.mvptest.model.Meal;

public interface IFetchMeal {
    void onLoadComplete(Meal meal);

    void onError();
}
