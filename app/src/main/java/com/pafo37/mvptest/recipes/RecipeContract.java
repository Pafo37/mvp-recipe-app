package com.pafo37.mvptest.recipes;

import com.pafo37.mvptest.mvp.BasePresenter;
import com.pafo37.mvptest.mvp.BaseView;

public interface RecipeContract {

    interface View extends BaseView<Presenter>{
        void setProgressIndicator(boolean active);

        void showStatistics(int numberOfIncompleteTasks, int numberOfCompletedTasks);

        void showLoadingStatisticsError();

        boolean isActive();
    }

    interface Presenter extends BasePresenter{

    }
}
