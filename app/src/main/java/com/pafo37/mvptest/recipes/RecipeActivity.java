package com.pafo37.mvptest.recipes;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.pafo37.mvptest.R;

public class RecipeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);
        RecipeFragment statisticsFragment = new RecipeFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.content, statisticsFragment, statisticsFragment.getTag());
        fragmentTransaction.commit();

        new RecipePresenter(statisticsFragment);
    }

}
