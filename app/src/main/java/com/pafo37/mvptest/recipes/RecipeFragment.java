package com.pafo37.mvptest.recipes;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pafo37.mvptest.R;
import com.pafo37.mvptest.db.Database;
import com.pafo37.mvptest.db.SqlSchema;
import com.pafo37.mvptest.model.Meal;

import java.util.ArrayList;
import java.util.List;

import static com.google.android.gms.common.internal.Preconditions.checkNotNull;
import static com.pafo37.mvptest.db.DbUtil.projection;
import static com.pafo37.mvptest.db.DbUtil.selection;
import static com.pafo37.mvptest.db.DbUtil.selectionArgs;
import static com.pafo37.mvptest.db.DbUtil.sortOrder;

public class RecipeFragment extends Fragment implements RecipeContract.View {
    private TextView mStatisticsTv;
    private RecipeContract.Presenter mPresenter;
    private Database database;
    SQLiteDatabase sqLiteDatabase;
    private TextView mTxtMeal;
    private TextView mTxtArea;
    private TextView mTxtInstructions;

    public static RecipeFragment newInstance() {
        return new RecipeFragment();
    }

    @Override
    public void setPresenter(@NonNull RecipeContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_recipes, container, false);
        mTxtMeal = root.findViewById(R.id.txt_meal_saved);
        mTxtArea = root.findViewById(R.id.txt_meal_area_saved);
        mTxtInstructions = root.findViewById(R.id.txt_meal_instructions_saved);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        database = new Database(getContext());
        sqLiteDatabase = database.getReadableDatabase();

        Cursor cursor = sqLiteDatabase.query(
                SqlSchema.MealEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        Log.e("log", String.valueOf(cursor.getColumnCount()));
        Meal meal = null;
        cursor.moveToNext();
        String mealId = cursor.getString(cursor.getColumnIndexOrThrow(SqlSchema.MealEntry._ID));
        String mealName = cursor.getString(cursor.getColumnIndexOrThrow(SqlSchema.MealEntry.COLUMN_MEAL_NAME));
        String mealArea = cursor.getString(cursor.getColumnIndexOrThrow(SqlSchema.MealEntry.COLUMN_MEAL_AREA));
        String mealInstructions = cursor.getString(cursor.getColumnIndexOrThrow(SqlSchema.MealEntry.COLUMN_MEAL_INSTRUCTIONS));
        meal = new Meal(mealId, mealName, mealArea, mealInstructions);
        if (meal != null) {
            mTxtMeal.setText(meal.getStrMeal());
            mTxtArea.setText(meal.getStrArea());
            mTxtInstructions.setText(meal.getStrInstructions());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setProgressIndicator(boolean active) {

    }

    @Override
    public void showStatistics(int numberOfIncompleteTasks, int numberOfCompletedTasks) {

    }

    @Override
    public void showLoadingStatisticsError() {

    }

    @Override
    public boolean isActive() {
        return false;
    }
}
