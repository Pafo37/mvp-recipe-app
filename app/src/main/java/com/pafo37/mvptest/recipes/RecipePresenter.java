package com.pafo37.mvptest.recipes;

import com.pafo37.mvptest.db.Database;

public class RecipePresenter implements RecipeContract.Presenter {

    private final RecipeContract.View mStatisticsView;

    @Override
    public void start() {

    }

    public RecipePresenter(RecipeContract.View mStatisticsView) {
        this.mStatisticsView = mStatisticsView;
        mStatisticsView.setPresenter(this);
    }
}
