package com.pafo37.mvptest.mvp;

public interface BaseView<T> {
    void setPresenter(T presenter);
}
