package com.pafo37.mvptest.searchrecipe;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.pafo37.mvptest.R;

public class SearchRecipeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_recipes);
        SearchRecipeFragment recipesFragment = new SearchRecipeFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.content, recipesFragment, recipesFragment.getTag());
        fragmentTransaction.commit();

        new SearchRecipesPresenter(recipesFragment);
    }
}
