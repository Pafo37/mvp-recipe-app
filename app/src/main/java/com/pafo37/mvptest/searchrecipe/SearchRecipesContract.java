package com.pafo37.mvptest.searchrecipe;

import com.pafo37.mvptest.model.Meal;
import com.pafo37.mvptest.mvp.BasePresenter;
import com.pafo37.mvptest.mvp.BaseView;

public interface SearchRecipesContract {

    interface View extends BaseView<SearchRecipesContract.Presenter> {
        void showMeal(Meal meal);
        void showError();
    }

    interface Presenter extends BasePresenter {
        void loadMeal(String meal);
    }
}
