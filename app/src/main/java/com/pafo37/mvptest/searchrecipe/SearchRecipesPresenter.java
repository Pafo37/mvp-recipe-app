package com.pafo37.mvptest.searchrecipe;

import com.pafo37.mvptest.api.FetchMealTask;
import com.pafo37.mvptest.api.IFetchMeal;
import com.pafo37.mvptest.model.Meal;

public class SearchRecipesPresenter implements SearchRecipesContract.Presenter, IFetchMeal {

    private final SearchRecipesContract.View mRecipesView;

    public SearchRecipesPresenter(SearchRecipesContract.View mRecipesView) {
        this.mRecipesView = mRecipesView;
        mRecipesView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void loadMeal(String meal) {
        FetchMealTask fetchMealTask = new FetchMealTask(this);
        fetchMealTask.execute(meal);
    }

    @Override
    public void onLoadComplete(Meal meal) {
        if (meal != null) {
            mRecipesView.showMeal(meal);
        } else {
            mRecipesView.showError();
        }
    }

    @Override
    public void onError() {

    }
}
