package com.pafo37.mvptest.searchrecipe;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pafo37.mvptest.R;
import com.pafo37.mvptest.db.Database;
import com.pafo37.mvptest.db.SqlSchema;
import com.pafo37.mvptest.model.Meal;
import com.pafo37.mvptest.recipes.RecipeActivity;

import static com.pafo37.mvptest.db.DbUtil.projection;
import static com.pafo37.mvptest.db.DbUtil.selection;
import static com.pafo37.mvptest.db.DbUtil.selectionArgs;
import static com.pafo37.mvptest.db.DbUtil.sortOrder;

public class SearchRecipeFragment extends Fragment implements SearchRecipesContract.View {

    private EditText mSearchRecipes;
    private TextView mTxtMeal;
    private TextView mTxtArea;
    private TextView mTxtInstructions;
    private Button mSearchButton;
    private Button mSaveRecipesButton;
    private Button mToRecipesButton;
    private SearchRecipesContract.Presenter mPresenter;
    private Meal meal;
    private Database database;
    SQLiteDatabase sqLiteDatabase;
    private long mealId = 0;

    @Override
    public void setPresenter(SearchRecipesContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search_recipes, container, false);
        mSearchRecipes = root.findViewById(R.id.search_text_view);
        mSearchButton = root.findViewById(R.id.button_search);
        mSaveRecipesButton = root.findViewById(R.id.button_save_recipe);
        mToRecipesButton = root.findViewById(R.id.button_to_recipe);
        mTxtMeal = root.findViewById(R.id.txt_meal_search_result);
        mTxtArea = root.findViewById(R.id.txt_meal_area_search_result);
        mTxtInstructions = root.findViewById(R.id.txt_meal_instructions_search_result);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        database = new Database(getContext());
        sqLiteDatabase = database.getWritableDatabase();

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mSearchRecipes.getText().toString())) {
                    mPresenter.loadMeal(mSearchRecipes.getText().toString());
                } else {
                    Toast.makeText(getContext(), "Please enter a meal.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mToRecipesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkDb()) {
                    Intent intent = new Intent(getActivity(), RecipeActivity.class);
                    intent.putExtra("MealId", mealId);
                    startActivity(intent);
                } else {
                    Toast.makeText(getContext(), "No recipe stored yet", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mSaveRecipesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (meal != null) {
                    ContentValues values = new ContentValues();
                    values.put(SqlSchema.MealEntry.COLUMN_MEAL_NAME, meal.getStrMeal());
                    values.put(SqlSchema.MealEntry.COLUMN_MEAL_AREA, meal.getStrArea());
                    values.put(SqlSchema.MealEntry.COLUMN_MEAL_INSTRUCTIONS, meal.getStrInstructions());
                    mealId = sqLiteDatabase.insert(SqlSchema.MealEntry.TABLE_NAME, null, values);
                    Toast.makeText(getContext(), "Recipe stored", Toast.LENGTH_SHORT).show();
                }
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    private boolean checkDb() {
        sqLiteDatabase = database.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.query(
                SqlSchema.MealEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        cursor.moveToNext();
        try {
            if (Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(SqlSchema.MealEntry._ID))) != 0) {
                return true;
            } else {
                return false;
            }
        } catch (CursorIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void showMeal(Meal meal) {
        if (isAdded()) {
            mTxtMeal.setText("Meal name:" + meal.getStrMeal());
            mTxtArea.setText("Meal area:" + meal.getStrArea());
            mTxtInstructions.setText("Meal instructions:" + meal.getStrInstructions());
            this.meal = meal;
        }
    }

    @Override
    public void showError() {
        Toast.makeText(getContext(), "Sorry, I couldn't find your meal", Toast.LENGTH_SHORT).show();
    }
}
