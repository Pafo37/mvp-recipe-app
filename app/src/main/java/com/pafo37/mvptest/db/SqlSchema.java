package com.pafo37.mvptest.db;

import android.provider.BaseColumns;

public final class SqlSchema {
    public SqlSchema() {
    }

    public static class MealEntry implements BaseColumns {
        public static final String TABLE_NAME = "meals";
        public static final String COLUMN_MEAL_NAME = "meal_name";
        public static final String COLUMN_MEAL_AREA = "meal_area";
        public static final String COLUMN_MEAL_INSTRUCTIONS = "meal_instructions";
    }
}
