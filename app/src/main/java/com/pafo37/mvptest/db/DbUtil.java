package com.pafo37.mvptest.db;

import android.provider.BaseColumns;

public class DbUtil {
    // Define a projection that specifies which columns from the database
    // you will actually use after this query.
    public static String[] projection = {
            BaseColumns._ID,
            SqlSchema.MealEntry.COLUMN_MEAL_NAME,
            SqlSchema.MealEntry.COLUMN_MEAL_AREA,
            SqlSchema.MealEntry.COLUMN_MEAL_INSTRUCTIONS
    };

    // Filter results WHERE "title" = 'My Title'
    public static String selection = BaseColumns._ID + " = ?";
    public static String[] selectionArgs = {"1"};

    // How you want the results sorted in the resulting Cursor
    public static String sortOrder =
            BaseColumns._ID + " DESC";
}
